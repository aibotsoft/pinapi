package pinapi_test

import (
	"context"
	"github.com/stretchr/testify/assert"
	"gitlab.com/aibotsoft/pinapi"
	"os"
	"testing"
)

var a *pinapi.ApiClient

func TestMain(t *testing.M) {
	var err error
	a, err = pinapi.NewApiClient(&pinapi.Config{
		URL:      os.Getenv("URL"),
		Username: os.Getenv("PINNACLE_USERNAME"),
		Password: os.Getenv("PINNACLE_PASS"),
		Debug:    true,
	})
	if err != nil {
		panic(err)
	}
	t.Run()
}

func TestGetClientBalance(t *testing.T) {
	got, err := a.GetClientBalance(context.Background())
	if assert.NoError(t, err, "TestGetClientBalance") {
		assert.Contains(t, []string{"USD", "EUR"}, got.Currency)
	}
}

func TestGetSports(t *testing.T) {
	got, err := a.GetSports(context.Background())
	if assert.NoError(t, err) {
		assert.Equal(t, int64(1), got.Sports[0].ID)
		assert.Equal(t, "Badminton", got.Sports[0].Name)
	}
}
func TestGetLeagues(t *testing.T) {
	got, err := a.GetLeagues(context.Background(), &pinapi.LeaguesRequest{SportId: 12})
	if assert.NoError(t, err) {
		assert.Equal(t, int64(12), got.Leagues[0].ID)
		assert.Equal(t, "All England Championships - Men Doubles", got.Leagues[0].Name)
	}
}
func TestGetPeriods(t *testing.T) {
	got, err := a.GetPeriods(context.Background(), &pinapi.PeriodsRequest{SportId: 29})
	if assert.NoError(t, err) {
		assert.Equal(t, "Match", got.Periods[0].Description)
		assert.Equal(t, int64(0), got.Periods[0].Number)
	}
}
func TestGetInRunning(t *testing.T) {
	got, err := a.GetInRunning(context.Background())
	if assert.NoError(t, err) {
		assert.Equal(t, int64(29), got.Sports[0].ID)
		//assert.Greater(t, got.Sports[0].Leagues[0].Events[0].ID, int64(0))
	}
}
func TestGetCancellationReasons(t *testing.T) {
	got, err := a.GetCancellationReasons(context.Background())
	if assert.NoError(t, err) {
		assert.Equal(t, "FBS_CW_1", got.CancellationReasons[0].Code)
		assert.Equal(t, "The wager was offered with an incorrect line", got.CancellationReasons[0].Description)
	}
}
func TestGetCurrencies(t *testing.T) {
	got, err := a.GetCurrencies(context.Background())
	if assert.NoError(t, err) {
		assert.Greater(t, got.Currencies[0].Rate, float64(0))
		assert.Equal(t, "AED", got.Currencies[0].Code)
	}
}
func TestBettingStatus(t *testing.T) {
	got, err := a.GetBettingStatus(context.Background())
	if assert.NoError(t, err) {
		assert.Equal(t, "ALL_BETTING_ENABLED", got.Status)
	}
}

func TestGetFixtures(t *testing.T) {
	var sportId int64 = 29
	got, err := a.GetFixtures(context.Background(), &pinapi.FixturesRequest{
		SportID: sportId,
		IsLive:  false,
	})
	if assert.NoError(t, err) {
		assert.Equal(t, sportId, got.SportID)
		assert.Greater(t, got.League[0].Events[0].ID, int64(1))
	}
}
func TestOdds(t *testing.T) {
	var sportId int64 = 29
	got, err := a.GetOdds(context.Background(), &pinapi.OddsRequest{
		SportID:    sportId,
		OddsFormat: pinapi.DECIMAL,
		Since:      1287328590,
		IsLive:     false,
	})
	if assert.NoError(t, err) {
		assert.Equal(t, sportId, got.SportID)
		assert.Greater(t, got.Leagues[0].Events[0].Periods[0].LineID, int64(1))
	}
}

func TestGetLine(t *testing.T) {
	got, err := a.GetLine(context.Background(), &pinapi.LineRequest{
		SportId:      29,
		LeagueId:     1792,
		EventId:      1360880044,
		OddsFormat:   pinapi.DECIMAL,
		BetType:      pinapi.MONEYLINE,
		PeriodNumber: 0,
		Team:         pinapi.TEAM1,
	})
	if assert.NoError(t, err) {
		assert.Equal(t, "NOT_EXISTS", got.Status)
	}
}

func TestGetBets(t *testing.T) {
	got, err := a.GetBets(context.Background(), &pinapi.BetsRequest{
		BetIDs: "1763441153,1763441150,1763426416,1763426414,1763426415,1763354719",
		//FromDate: time.Now().Format(time.RFC3339),
		//ToDate: time.Now().Format(time.RFC3339),
	})
	if assert.NoError(t, err) {
		assert.Equal(t, "NOT_EXISTS", got)
	}
}
func TestPlaceStraightBet(t *testing.T) {
	got, err := a.PlaceStraightBet(context.Background(), &pinapi.PlaceStraightBetRequest{
		OddsFormat:       pinapi.DECIMAL,
		UniqueRequestID:  "cbe64ec1-ab9f-4687-b9c0-10a0f317ddb4",
		AcceptBetterLine: true,
		EventID:          1360329054,
		LineID:           1394273022,
		WinRiskStake:     pinapi.Risk,
		FillType:         pinapi.Normal,
		Stake:            1,
		BetType:          pinapi.MONEYLINE,
		PeriodNumber:     0,
		Team:             pinapi.TEAM1,
	})
	if assert.NoError(t, err) {
		assert.Equal(t, "NOT_EXISTS", got.Status)
	}
}
