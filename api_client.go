package pinapi

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/google/go-querystring/query"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"time"
)

const defaultUserAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36"
const defaultHost = "https://api.pinnacle.com/"

type Config struct {
	Username  string
	Password  string
	URL       string
	UserAgent string
	Debug     bool
	Client    *http.Client
}

type ApiClient struct {
	client *http.Client
	config *Config
}

func NewApiClient(config *Config) (*ApiClient, error) {
	if config.Username == "" || config.Password == "" {
		return nil, errors.New("username or password can't be empty")
	}
	if config.URL == "" {
		config.URL = defaultHost
	}
	if config.UserAgent == "" {
		config.UserAgent = defaultUserAgent
	}
	client := config.Client
	if config.Client == nil {
		client = &http.Client{}
	}
	return &ApiClient{client: client, config: config}, nil
}

func (a *ApiClient) Send(ctx context.Context, target interface{}, requestMethod string, path string, params interface{}, data interface{}) (err error) {
	u, err := url.Parse(a.config.URL)
	if err != nil {
		return fmt.Errorf("url.Parse error: %w", err)
	}
	u.Path = path

	if params != nil {
		v, err := query.Values(params)
		if err != nil {
			return fmt.Errorf("query.Values error: %w", err)
		}
		u.RawQuery = v.Encode()
	}
	payloadBuf := new(bytes.Buffer)
	if data != nil {
		err := json.NewEncoder(payloadBuf).Encode(data)
		if err != nil {
			return fmt.Errorf("encode post data error: %w", err)
		}
	}
	request, err := http.NewRequestWithContext(ctx, requestMethod, u.String(), payloadBuf)
	if err != nil {
		return fmt.Errorf("create request error: %w", err)
	}
	request.Header.Add("Accept", "application/json")
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("User-Agent", a.config.UserAgent)
	request.SetBasicAuth(a.config.Username, a.config.Password)

	start := time.Now()
	if a.config.Debug {
		dump, err := httputil.DumpRequestOut(request, true)
		if err != nil {
			return fmt.Errorf("httputil.DumpRequestOut error: %w", err)
		}
		log.Printf("\n%s", string(dump))
	}

	resp, err := a.client.Do(request)
	if err != nil {
		return fmt.Errorf("request error: %w", err)
	}
	defer func() {
		err2 := resp.Body.Close()
		if err2 != nil {
			log.Println("close body error", err2)
		}
	}()

	if a.config.Debug {
		dump, err := httputil.DumpResponse(resp, true)
		//_ = ioutil.WriteFile("test.json", dump, os.ModePerm)
		if err != nil {
			return fmt.Errorf("httputil.DumpResponse error: %w", err)
		}
		log.Printf("\nTotalTime: %s\n%s", time.Since(start), string(dump))
	}

	if resp.StatusCode != 200 {
		var errorData = new(PinnacleError)
		err = json.NewDecoder(resp.Body).Decode(errorData)
		if err != nil {
			return fmt.Errorf("decode errorData error: %w", err)
		}
		return errorData
	}
	err = json.NewDecoder(resp.Body).Decode(target)
	if err != nil {
		return fmt.Errorf("decode body error: %w", err)
	}
	return nil
}
