package pinapi

import (
	"context"
)

func (a *ApiClient) GetFixtures(ctx context.Context, r *FixturesRequest) (res *FixturesResponse, err error) {
	err = a.Send(ctx, &res, "GET", "v3/fixtures", r, nil)
	return
}
func (a *ApiClient) GetFixturesSpecial(ctx context.Context, r *FixturesSpecialRequest) (res *FixturesSpecialResponse, err error) {
	err = a.Send(ctx, &res, "GET", "v1/fixtures/special", r, nil)
	return
}
func (a *ApiClient) GetFixturesSettled(ctx context.Context, r *FixturesSettledRequest) (res *FixturesSettledResponse, err error) {
	err = a.Send(ctx, &res, "GET", "v1/fixtures/settled", r, nil)
	return
}
func (a *ApiClient) GetFixturesSpecialSettled(ctx context.Context, r *FixturesSpecialSettledRequest) (res *FixturesSpecialSettledResponse, err error) {
	err = a.Send(ctx, &res, "GET", "v1/fixtures/special/settled", r, nil)
	return
}
func (a *ApiClient) GetLine(ctx context.Context, r *LineRequest) (res *LineResponse, err error) {
	err = a.Send(ctx, &res, "GET", "v2/line", r, nil)
	return
}
func (a *ApiClient) GetLineParlay(ctx context.Context, r *LineParlayRequest) (res *LineParlayResponse, err error) {
	err = a.Send(ctx, &res, "POST", "v1/line/parlay", nil, r)
	return
}
func (a *ApiClient) GetLineTeaser(ctx context.Context, r *LineTeaserRequest) (res *LineTeaserResponse, err error) {
	err = a.Send(ctx, &res, "POST", "v1/line/teaser", nil, r)
	return
}
func (a *ApiClient) GetSpecialLine(ctx context.Context, r *SpecialLineRequest) (res *LineSpecialResponse, err error) {
	err = a.Send(ctx, &res, "GET", "v1/line/special", r, nil)
	return res, err
}
func (a *ApiClient) GetSports(ctx context.Context) (res *SportsResponse, err error) {
	err = a.Send(ctx, &res, "GET", "v3/sports", nil, nil)
	return
}
func (a *ApiClient) GetLeagues(ctx context.Context, r *LeaguesRequest) (res *LeaguesResponse, err error) {
	err = a.Send(ctx, &res, "GET", "v3/leagues", r, nil)
	return
}
func (a *ApiClient) GetPeriods(ctx context.Context, r *PeriodsRequest) (res *PeriodsResponse, err error) {
	err = a.Send(ctx, &res, "GET", "v1/periods", r, nil)
	return
}
func (a *ApiClient) GetInRunning(ctx context.Context) (res *InrunningResponse, err error) {
	err = a.Send(ctx, &res, "GET", "v2/inrunning", nil, nil)
	return
}
func (a *ApiClient) GetTeaserGroups(ctx context.Context, r *TeaserGroupsRequest) (res *TeaserGroupsResponse, err error) {
	err = a.Send(ctx, &res, "GET", "v1/teaser/groups", r, nil)
	return
}
func (a *ApiClient) GetCancellationReasons(ctx context.Context) (res *CancellationReasonsResponse, err error) {
	err = a.Send(ctx, &res, "GET", "v1/cancellationreasons", nil, nil)
	return
}
func (a *ApiClient) GetCurrencies(ctx context.Context) (res *CurrenciesResponse, err error) {
	err = a.Send(ctx, &res, "GET", "v2/currencies", nil, nil)
	return
}
func (a *ApiClient) GetOdds(ctx context.Context, r *OddsRequest) (res *OddsResponse, err error) {
	err = a.Send(ctx, &res, "GET", "v3/odds", r, nil)
	return
}
func (a *ApiClient) GetOddsParlay(ctx context.Context, r *OddsRequest) (res *OddsParlayResponse, err error) {
	err = a.Send(ctx, &res, "GET", "v1/odds/parlay", r, nil)
	return
}
func (a *ApiClient) GetOddsTeaser(ctx context.Context, r *OddsTeaserRequest) (res *OddsTeaserResponse, err error) {
	err = a.Send(ctx, &res, "GET", "v1/odds/parlay", r, nil)
	return
}
func (a *ApiClient) GetOddsSpecial(ctx context.Context, r *OddsSpecialRequest) (res *OddsSpecialResponse, err error) {
	err = a.Send(ctx, &res, "GET", "v1/odds/special", r, nil)
	return
}
func (a *ApiClient) GetClientBalance(ctx context.Context) (res *ClientBalanceResponse, err error) {
	err = a.Send(ctx, &res, "GET", "v1/client/balance", nil, nil)
	return
}
func (a *ApiClient) PlaceStraightBet(ctx context.Context, r *PlaceStraightBetRequest) (res *PlaceStraightBetResponse, err error) {
	err = a.Send(ctx, &res, "POST", "v2/bets/place", nil, r)
	return
}
func (a *ApiClient) PlaceParlayBet(ctx context.Context, r *ParlayBetRequest) (res *PlaceParlayBetResponse, err error) {
	err = a.Send(ctx, res, "POST", "v1/bets/parlay", nil, r)
	return res, err
}
func (a *ApiClient) PlaceTeaserBet(ctx context.Context, r *TeaserBetRequest) (res *TeaserBetResponse, err error) {
	err = a.Send(ctx, &res, "POST", "v1/bets/teaser", nil, r)
	return res, err
}
func (a *ApiClient) GetBets(ctx context.Context, r *BetsRequest) (res *BetsResponse, err error) {
	err = a.Send(ctx, &res, "GET", "v3/bets", r, nil)
	return
}
func (a *ApiClient) GetBettingStatus(ctx context.Context) (res *BettingStatusResponse, err error) {
	err = a.Send(ctx, &res, "GET", "v1/bets/betting-status", nil, nil)
	return
}

// todo: implement special bet.
