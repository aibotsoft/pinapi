package pinapi

import (
	"fmt"
	"time"
)

type PinnacleError struct {
	Ref     string `json:"ref,omitempty"`
	Code    string `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
}

func (p *PinnacleError) Error() string {
	return fmt.Sprintf("[%s]: %s", p.Code, p.Message)
}

type ClientBalanceResponse struct {
	AvailableBalance        float32 `json:"availableBalance"`
	OutstandingTransactions float32 `json:"outstandingTransactions"`
	GivenCredit             float32 `json:"givenCredit"`
	Currency                string  `json:"currency"`
}

type LeaguesRequest struct {
	SportId int `url:"sportId,omitempty"`
}
type PeriodsRequest struct {
	SportId int `url:"sportId,omitempty"`
}
type OddsTeaserRequest struct {
	TeaserId int `url:"teaserId,omitempty"`
}
type TeaserGroupsRequest struct {
	OddsFormat OddsFormat `url:"oddsFormat,omitempty"`
}

type BetsRequest struct {
	BetList     BetListType     `url:"betlist,omitempty"`
	BetStatuses BetStatusesType `url:"betStatuses,omitempty"`

	FromDate   string      `url:"fromDate,omitempty"`
	ToDate     string      `url:"toDate,omitempty"`
	SortDir    SortDirType `url:"sortDir,omitempty"`
	PageSize   int         `url:"pageSize,omitempty"`
	FromRecord int         `url:"fromRecord,omitempty"`
	//BetIDs           []int       `url:"betids,omitempty"`
	BetIDs           string   `url:"betids,omitempty"`
	UniqueRequestIds []string `url:"uniqueRequestIds,omitempty"`
}

// res
type BetsResponse struct {
	MoreAvailable bool          `json:"moreAvailable,omitempty"`
	PageSize      int64         `json:"pageSize,omitempty"`
	FromRecord    int64         `json:"fromRecord,omitempty"`
	ToRecord      int64         `json:"toRecord,omitempty"`
	StraightBets  []StraightBet `json:"straightBets"`
	ParlayBets    []ParlayBet   `json:"parlayBets,omitempty"`
	TeaserBets    []TeaserBet   `json:"teaserBets,omitempty"`
	SpecialBets   []SpecialBet  `json:"specialBets,omitempty"`
	ManualBets    []ManualBet   `json:"manualBets,omitempty"`
}

type ManualBet struct {
	BetID          int64           `json:"betId"`
	WagerNumber    int64           `json:"wagerNumber"`
	PlacedAt       time.Time       `json:"placedAt"`
	BetStatus      BetStatusesType `json:"betStatus"`
	BetType        BetType         `json:"betType"`
	Win            float64         `json:"win"`
	Risk           float64         `json:"risk"`
	WinLoss        float64         `json:"winLoss"`
	UpdateSequence int64           `json:"updateSequence"`
	Description    string          `json:"description"`
	ReferenceBetID *int64          `json:"referenceBetId"`
}

type ParlayBet struct {
	BetID              int64              `json:"betId"`
	UniqueRequestID    string             `json:"uniqueRequestId"`
	WagerNumber        int64              `json:"wagerNumber"`
	PlacedAt           string             `json:"placedAt"`
	BetStatus          BetStatusesType    `json:"betStatus"`
	BetType            BetType            `json:"betType"`
	Win                float64            `json:"win"`
	Risk               int64              `json:"risk"`
	WinLoss            *float64           `json:"winLoss"`
	OddsFormat         string             `json:"oddsFormat"`
	CustomerCommission *float64           `json:"customerCommission"`
	CancellationReason CancellationReason `json:"cancellationReason"`
	UpdateSequence     int64              `json:"updateSequence"`
	Legs               []ParlayBetLeg     `json:"legs"`
	Price              float64            `json:"price"`
	FinalPrice         float64            `json:"finalPrice"`
}

//type ParlayBet struct {
//	BetID              int64               `json:"betId"`
//	UniqueRequestID    string              `json:"uniqueRequestId"`
//	WagerNumber        int64               `json:"wagerNumber"`
//	PlacedAt           string              `json:"placedAt"`
//	BetStatus          string              `json:"betStatus"`
//	BetType            string              `json:"betType"`
//	Win                float64             `json:"win"`
//	Risk               int64               `json:"risk"`
//	WinLoss            *float64            `json:"winLoss"`
//	OddsFormat         string              `json:"oddsFormat"`
//	CustomerCommission *float64            `json:"customerCommission"`
//	CancellationReason CancellationReason2 `json:"cancellationReason"`
//	UpdateSequence     int64               `json:"updateSequence"`
//	Legs               []Leg               `json:"legs"`
//	Price              int64               `json:"price"`
//	FinalPrice         int64               `json:"finalPrice"`
//}

//type Detail struct {
//	Key   string `json:"key"`
//	Value string `json:"value"`
//}

type ParlayBetLeg struct {
	SportID            int64              `json:"sportId"`
	LegBetType         string             `json:"legBetType"`
	LegBetStatus       BetStatusesType    `json:"legBetStatus"`
	LeagueID           int64              `json:"leagueId"`
	EventID            int64              `json:"eventId"`
	EventStartTime     string             `json:"eventStartTime"`
	Handicap           *float64           `json:"handicap"`
	Price              int64              `json:"price"`
	TeamName           string             `json:"teamName"`
	Side               *Side              `json:"side"`
	Pitcher1           *string            `json:"pitcher1"`
	Pitcher2           *string            `json:"pitcher2"`
	Pitcher1MustStart  bool               `json:"pitcher1MustStart"`
	Pitcher2MustStart  bool               `json:"pitcher2MustStart"`
	Team1              string             `json:"team1"`
	Team2              string             `json:"team2"`
	PeriodNumber       int64              `json:"periodNumber"`
	FtTeam1Score       *float64           `json:"ftTeam1Score"`
	FtTeam2Score       *float64           `json:"ftTeam2Score"`
	PTeam1Score        *float64           `json:"pTeam1Score"`
	PTeam2Score        *float64           `json:"pTeam2Score"`
	CancellationReason CancellationReason `json:"cancellationReason"`
}

type SpecialBet struct {
	BetID              int64              `json:"betId"`
	UniqueRequestID    string             `json:"uniqueRequestId"`
	WagerNumber        int64              `json:"wagerNumber"`
	PlacedAt           string             `json:"placedAt"`
	BetStatus          string             `json:"betStatus"`
	BetType            string             `json:"betType"`
	Win                int64              `json:"win"`
	Risk               float64            `json:"risk"`
	WinLoss            *float64           `json:"winLoss"`
	OddsFormat         string             `json:"oddsFormat"`
	CustomerCommission *float64           `json:"customerCommission"`
	CancellationReason CancellationReason `json:"cancellationReason"`
	UpdateSequence     int64              `json:"updateSequence"`
	SpecialID          int64              `json:"specialId"`
	SpecialName        string             `json:"specialName"`
	ContestantID       int64              `json:"contestantId"`
	ContestantName     string             `json:"contestantName"`
	Price              int64              `json:"price"`
	Handicap           float64            `json:"handicap"`
	Units              string             `json:"units"`
	SportID            int64              `json:"sportId"`
	LeagueID           int64              `json:"leagueId"`
	EventID            *int64             `json:"eventId"`
	PeriodNumber       *int64             `json:"periodNumber"`
	Team1              *string            `json:"team1"`
	Team2              *string            `json:"team2"`
	EventStartTime     string             `json:"eventStartTime"`
}

//type StraightBet struct {
//	BetID              int64              `json:"betId"`
//	WagerNumber        int64              `json:"wagerNumber"`
//	PlacedAt           string             `json:"placedAt"`
//	BetStatus          string             `json:"betStatus"`
//	BetType            string             `json:"betType"`
//	Win                int64              `json:"win"`
//	Risk               float64            `json:"risk"`
//	WinLoss            *float64           `json:"winLoss"`
//	OddsFormat         string             `json:"oddsFormat"`
//	CustomerCommission *float64           `json:"customerCommission"`
//	CancellationReason CancellationReason `json:"cancellationReason"`
//	UpdateSequence     int64              `json:"updateSequence"`
//	SportID            int64              `json:"sportId"`
//	LeagueID           int64              `json:"leagueId"`
//	EventID            int64              `json:"eventId"`
//	Handicap           *float64           `json:"handicap"`
//	Price              int64              `json:"price"`
//	TeamName           string             `json:"teamName"`
//	Side               *models.Side       `json:"side"`
//	Pitcher1           *string            `json:"pitcher1"`
//	Pitcher2           *string            `json:"pitcher2"`
//	Pitcher1MustStart  bool               `json:"pitcher1MustStart"`
//	Pitcher2MustStart  bool               `json:"pitcher2MustStart"`
//	Team1              string             `json:"team1"`
//	Team2              string             `json:"team2"`
//	PeriodNumber       int64              `json:"periodNumber"`
//	Team1Score         *float64           `json:"team1Score"`
//	Team2Score         *float64           `json:"team2Score"`
//	FtTeam1Score       *float64           `json:"ftTeam1Score"`
//	FtTeam2Score       *float64           `json:"ftTeam2Score"`
//	PTeam1Score        *float64           `json:"pTeam1Score"`
//	PTeam2Score        *float64           `json:"pTeam2Score"`
//	IsLive             bool               `json:"isLive"`
//	EventStartTime     string             `json:"eventStartTime"`
//}

//type TeaserBet struct {
//	BetID              int64              `json:"betId"`
//	UniqueRequestID    string             `json:"uniqueRequestId"`
//	WagerNumber        int64              `json:"wagerNumber"`
//	PlacedAt           string             `json:"placedAt"`
//	BetStatus          string             `json:"betStatus"`
//	BetType            string             `json:"betType"`
//	Win                int64              `json:"win"`
//	Risk               int64              `json:"risk"`
//	WinLoss            int64              `json:"winLoss"`
//	OddsFormat         string             `json:"oddsFormat"`
//	CustomerCommission int64              `json:"customerCommission"`
//	CancellationReason CancellationReason `json:"cancellationReason"`
//	UpdateSequence     int64              `json:"updateSequence"`
//	TeaserName         string             `json:"teaserName"`
//	IsSameEventOnly    bool               `json:"isSameEventOnly"`
//	MinPicks           int64              `json:"minPicks"`
//	MaxPicks           int64              `json:"maxPicks"`
//	Price              int64              `json:"price"`
//	FinalPrice         int64              `json:"finalPrice"`
//	TeaserID           int64              `json:"teaserId"`
//	TeaserGroupID      int64              `json:"teaserGroupId"`
//	Legs               []TeaserBetLeg     `json:"legs"`
//}

type TeaserBetLeg struct {
	SportID        int64  `json:"sportId"`
	LegBetType     string `json:"legBetType"`
	LegBetStatus   string `json:"legBetStatus"`
	LeagueID       int64  `json:"leagueId"`
	EventID        int64  `json:"eventId"`
	EventStartTime string `json:"eventStartTime"`
	Handicap       int64  `json:"handicap"`
	TeamName       string `json:"teamName"`
	Side           string `json:"side"`
	Team1          string `json:"team1"`
	Team2          string `json:"team2"`
	PeriodNumber   int64  `json:"periodNumber"`
}

type PlaceStraightBetRequest struct {
	OddsFormat        OddsFormat  `json:"oddsFormat"`
	UniqueRequestID   string      `json:"uniqueRequestId"`
	AcceptBetterLine  bool        `json:"acceptBetterLine"`
	Stake             float64     `json:"stake"`
	WinRiskStake      WinRiskType `json:"winRiskStake"`
	LineID            int64       `json:"lineId"`
	AltLineID         *int64      `json:"altLineId,omitempty"`
	Pitcher1MustStart bool        `json:"pitcher1MustStart,omitempty"`
	Pitcher2MustStart bool        `json:"pitcher2MustStart,omitempty"`
	FillType          FillType    `json:"fillType"`
	SportID           int64       `json:"sportId"`
	EventID           int64       `json:"eventId"`
	PeriodNumber      int64       `json:"periodNumber"`
	BetType           BetType     `json:"betType"`
	Team              TeamType    `json:"team,omitempty"`
	Side              Side        `json:"side,omitempty"`
}

type PSBStatus string

// "ACCEPTED" "PENDING_ACCEPTANCE" "PROCESSED_WITH_ERROR"
const (
	PSBStatus_ACCEPTED             PSBStatus = "ACCEPTED"
	PSBStatus_PENDING_ACCEPTANCE   PSBStatus = "PENDING_ACCEPTANCE"
	PSBStatus_PROCESSED_WITH_ERROR PSBStatus = "PROCESSED_WITH_ERROR"
)

type PlaceStraightBetResponse struct {
	Status string `json:"status"`

	//
	//string Nullable
	//Enum:"ALL_BETTING_CLOSED" "ALL_LIVE_BETTING_CLOSED" "ABOVE_EVENT_MAX" "ABOVE_MAX_BET_AMOUNT" "BELOW_MIN_BET_AMOUNT" "BLOCKED_BETTING" "BLOCKED_CLIENT" "INSUFFICIENT_FUNDS" "INVALID_COUNTRY" "INVALID_EVENT" "INVALID_ODDS_FORMAT" "LINE_CHANGED" "LISTED_PITCHERS_SELECTION_ERROR" "OFFLINE_EVENT" "PAST_CUTOFFTIME" "RED_CARDS_CHANGED" "SCORE_CHANGED" "TIME_RESTRICTION" "DUPLICATE_UNIQUE_REQUEST_ID" "INCOMPLETE_CUSTOMER_BETTING_PROFILE" "INVALID_CUSTOMER_PROFILE" "LIMITS_CONFIGURATION_ISSUE" "RESPONSIBLE_BETTING_LOSS_LIMIT_EXCEEDED" "RESPONSIBLE_BETTING_RISK_LIMIT_EXCEEDED" "SYSTEM_ERROR_3" "LICENCE_RESTRICTION_LIVE_BETTING_BLOCKED"
	ErrorCode       *string      `json:"errorCode"`
	UniqueRequestID string       `json:"uniqueRequestId"`
	StraightBet     *StraightBet `json:"straightBet"`
}

type StraightBet struct {
	BetID              int64              `json:"betId"`
	WagerNumber        int64              `json:"wagerNumber"`
	PlacedAt           time.Time          `json:"placedAt"`
	BetStatus          BetStatusesType    `json:"betStatus"`
	BetStatus2         BetStatusesType    `json:"betStatus2"`
	BetType            BetType            `json:"betType"`
	Win                float64            `json:"win"`
	Risk               float64            `json:"risk"`
	WinLoss            *float64           `json:"winLoss"`
	OddsFormat         OddsFormat         `json:"oddsFormat"`
	CustomerCommission *float64           `json:"customerCommission,omitempty"`
	CancellationReason CancellationReason `json:"cancellationReason"`
	UpdateSequence     int64              `json:"updateSequence"`
	SportID            int64              `json:"sportId"`
	SportName          string             `json:"sportName"`
	LeagueID           int64              `json:"leagueId"`
	LeagueName         string             `json:"leagueName"`
	EventID            int64              `json:"eventId"`
	Handicap           *float64           `json:"handicap"`
	Price              float64            `json:"price"`
	TeamName           string             `json:"teamName"`
	Side               *Side              `json:"side"`
	Pitcher1           *string            `json:"pitcher1,omitempty"`
	Pitcher2           *string            `json:"pitcher2,omitempty"`
	Pitcher1MustStart  *string            `json:"pitcher1MustStart,omitempty"`
	Pitcher2MustStart  *string            `json:"pitcher2MustStart,omitempty"`
	Team1              string             `json:"team1"`
	Team2              string             `json:"team2"`
	PeriodNumber       int64              `json:"periodNumber"`
	Team1Score         *float64           `json:"team1Score,omitempty"`
	Team2Score         *float64           `json:"team2Score,omitempty"`
	FtTeam1Score       *float64           `json:"ftTeam1Score,omitempty"`
	FtTeam2Score       *float64           `json:"ftTeam2Score,omitempty"`
	PTeam1Score        *float64           `json:"pTeam1Score,omitempty"`
	PTeam2Score        *float64           `json:"pTeam2Score,omitempty"`
	IsLive             interface{}        `json:"isLive"`
	EventStartTime     time.Time          `json:"eventStartTime"`
	SettledAt          *time.Time         `json:"settledAt,omitempty"`
}

type Detail struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

// req & res for bet parlay
type ParlayBetRequest struct {
	UniqueRequestID  string      `url:"uniqueRequestId,omitempty"`
	AcceptBetterLine bool        `url:"acceptBetterLine,omitempty"`
	RiskAmount       float64     `url:"riskAmount,omitempty"`
	OddsFormat       OddsFormat  `url:"oddsFormat,omitempty"`
	Legs             []LegParlay `url:"legs,omitempty"`

	//Array of string
	//Items Enum:"Parlay" "TwoLegRoundRobin" "ThreeLegRoundRobin" "FourLegRoundRobin" "FiveLegRoundRobin" "SixLegRoundRobin" "SevenLegRoundRobin" "EightLegRoundRobin"
	RoundRobinOptions []string `url:"roundRobinOptions,omitempty"`
}

type LegParlay struct {
	UniqueLegID       string  `url:"uniqueLegId,omitempty"`
	LineID            int64   `url:"lineId,omitempty"`
	AltLineID         *int64  `url:"altLineId,omitempty"`
	Pitcher1MustStart bool    `url:"pitcher1MustStart,omitempty"`
	Pitcher2MustStart bool    `url:"pitcher2MustStart,omitempty"`
	SportID           int64   `url:"sportId,omitempty"`
	EventID           int64   `url:"eventId,omitempty"`
	PeriodNumber      int64   `url:"periodNumber,omitempty"` // 0 - Game, 1 - 1st Half and 2 - 2nd Half
	LegBetType        BetType `url:"legBetType,omitempty"`
	Team              string  `url:"team,omitempty"`
	Side              *Side   `url:"side,omitempty"`
}

type PlaceParlayBetResponse struct {
	Status                   string                    `json:"status"`
	ErrorCode                *string                   `json:"errorCode"`
	BetID                    int64                     `json:"betId"`
	UniqueRequestID          string                    `json:"uniqueRequestId"`
	RoundRobinOptionWithOdds []RoundRobinOptionWithOdd `json:"roundRobinOptionWithOdds"`
	MaxRiskStake             int64                     `json:"maxRiskStake"`
	MinRiskStake             int64                     `json:"minRiskStake"`
	ValidLegs                []ValidLeg                `json:"validLegs"`
	InvalidLegs              []ValidLeg                `json:"invalidLegs"`
	ParlayBet                ParlayBet                 `json:"parlayBet"`
}

type ValidLeg struct {
	Status         string   `json:"status"`
	ErrorCode      *string  `json:"errorCode"`
	LegID          string   `json:"legId"`
	LineID         int64    `json:"lineId"`
	AltLineID      *int64   `json:"altLineId"`
	Price          int64    `json:"price"`
	CorrelatedLegs []string `json:"correlatedLegs"`
}

type Leg struct {
	SportID            int64              `json:"sportId"`
	LegBetType         string             `json:"legBetType"`
	LegBetStatus       string             `json:"legBetStatus"`
	LeagueID           int64              `json:"leagueId"`
	EventID            int64              `json:"eventId"`
	EventStartTime     string             `json:"eventStartTime"`
	Handicap           *float64           `json:"handicap"`
	Price              int64              `json:"price"`
	TeamName           string             `json:"teamName"`
	Side               *Side              `json:"side"`
	Pitcher1           *string            `json:"pitcher1"`
	Pitcher2           *string            `json:"pitcher2"`
	Pitcher1MustStart  bool               `json:"pitcher1MustStart"`
	Pitcher2MustStart  bool               `json:"pitcher2MustStart"`
	Team1              string             `json:"team1"`
	Team2              string             `json:"team2"`
	PeriodNumber       int64              `json:"periodNumber"`
	FtTeam1Score       *float64           `json:"ftTeam1Score"`
	FtTeam2Score       *float64           `json:"ftTeam2Score"`
	PTeam1Score        *float64           `json:"pTeam1Score"`
	PTeam2Score        *float64           `json:"pTeam2Score"`
	CancellationReason CancellationReason `json:"cancellationReason"`
}

type RoundRobinOptionWithOdd struct {
	RoundRobinOption     string  `json:"roundRobinOption"`
	Odds                 int64   `json:"odds"`
	UnroundedDecimalOdds float64 `json:"unroundedDecimalOdds"`
}

////////////////
type TeaserBetRequest struct {
	UniqueRequestID string      `json:"uniqueRequestId"`
	TeaserID        int64       `json:"teaserId"`
	OddsFormat      string      `json:"oddsFormat"`
	WinRiskStake    string      `json:"winRiskStake"`
	Stake           int64       `json:"stake"`
	Legs            []LegTeaser `json:"legs"`
}

//type LegTeaser struct {
//	LegID   string  `json:"legId"`
//	BetType BetType `json:"betType"`
//	LineID  int64   `json:"lineId"`
//	EventID int64   `json:"eventId"`
//	Team    string  `json:"team"`
//	Side    Side    `json:"side"`
//}

type TeaserBetResponse struct {
	Status          string      `json:"status"`
	ErrorCode       string      `json:"errorCode"`
	BetID           int64       `json:"betId"`
	UniqueRequestID string      `json:"uniqueRequestId"`
	Price           int64       `json:"price"`
	RiskAmount      int64       `json:"riskAmount"`
	WinAmount       int64       `json:"winAmount"`
	WinRiskStake    string      `json:"winRiskStake"`
	InvalidLegs     []ValidLeg2 `json:"invalidLegs"`
	ValidLegs       []ValidLeg2 `json:"validLegs"`
	TeaserBet       TeaserBet   `json:"teaserBet"`
}

type ValidLeg2 struct {
	Status    string `json:"status"`
	ErrorCode string `json:"errorCode"`
	LegID     string `json:"legId"`
	LineID    int64  `json:"lineId"`
	Points    int64  `json:"points"`
}

type TeaserBet struct {
	BetID              int64              `json:"betId"`
	UniqueRequestID    string             `json:"uniqueRequestId"`
	WagerNumber        int64              `json:"wagerNumber"`
	PlacedAt           string             `json:"placedAt"`
	BetStatus          BetStatusesType    `json:"betStatus"`
	BetType            BetType            `json:"betType"`
	Win                int64              `json:"win"`
	Risk               int64              `json:"risk"`
	WinLoss            int64              `json:"winLoss"`
	OddsFormat         OddsFormat         `json:"oddsFormat"`
	CustomerCommission int64              `json:"customerCommission"`
	CancellationReason CancellationReason `json:"cancellationReason"`
	UpdateSequence     int64              `json:"updateSequence"`
	TeaserName         string             `json:"teaserName"`
	IsSameEventOnly    bool               `json:"isSameEventOnly"`
	MinPicks           int64              `json:"minPicks"`
	MaxPicks           int64              `json:"maxPicks"`
	Price              int64              `json:"price"`
	FinalPrice         int64              `json:"finalPrice"`
	TeaserID           int64              `json:"teaserId"`
	TeaserGroupID      int64              `json:"teaserGroupId"`
	Legs               []Leg3             `json:"legs"`
}

type Leg3 struct {
	SportID        int64  `json:"sportId"`
	LegBetType     string `json:"legBetType"`
	LegBetStatus   string `json:"legBetStatus"`
	LeagueID       int64  `json:"leagueId"`
	EventID        int64  `json:"eventId"`
	EventStartTime string `json:"eventStartTime"`
	Handicap       int64  `json:"handicap"`
	TeamName       string `json:"teamName"`
	Side           string `json:"side"`
	Team1          string `json:"team1"`
	Team2          string `json:"team2"`
	PeriodNumber   int64  `json:"periodNumber"`
}

type BettingStatusResponse struct {
	Status string
}

type FixturesResponse struct {
	SportID int64            `json:"sportId"`
	Last    int64            `json:"last"`
	League  []FixturesLeague `json:"league"`
}

type FixturesLeague struct {
	ID     int64   `json:"id"`
	Name   string  `json:"name"`
	Events []Event `json:"events"`
}

type Event struct {
	ID       int64     `json:"id"`
	ParentID *int64    `json:"parentId,omitempty"`
	Starts   time.Time `json:"starts"`
	Home     string    `json:"home"`
	Away     string    `json:"away"`
	//Deprecated
	RotNum string `json:"rotNum"`
	//0 = No live betting will be offered on this event, 1 = Live betting event, 2 = Live betting will be offered on this match, but on a different event
	LiveStatus int64 `json:"liveStatus"`
	//Home team pitcher. Only for Baseball.
	HomePitcher *string `json:"homePitcher,omitempty"`
	//Away team pitcher. Only for Baseball.
	AwayPitcher *string `json:"awayPitcher,omitempty"`
	//Deprecated
	Status            string `json:"status"`
	ParlayRestriction int64  `json:"parlayRestriction"`
	AltTeaser         bool   `json:"altTeaser"`
	ResultingUnit     string `json:"resultingUnit"`
	Version           int64  `json:"version"`
}

// request
type FixturesRequest struct {
	SportID   int64 `json:"sportId"`
	LeagueIDs []int `json:"leagueIds,omitempty"`
	IsLive    bool  `json:"isLive,omitempty"`
	Since     int64 `json:"since,omitempty"`
	EventIDs  []int `json:"eventIds,omitempty"`
}

// ************************
// *** special fixtures ******
type FixturesSpecialResponse struct {
	SportID int64           `json:"sportId"`
	Last    int64           `json:"last"`
	Leagues []LeagueSpecial `json:"leagues"`
}

//type LeagueSpecial struct {
//	ID       int64     `json:"id"`
//	Specials []Special `json:"specials"`
//}
//
//type Special struct {
//	ID          int64        `json:"id"`
//	BetType     string       `json:"betType"`
//	Name        string       `json:"name"`
//	Date        string       `json:"date"`
//	Cutoff      string       `json:"cutoff"`
//	Category    string       `json:"category"`
//	Units       string       `json:"units"`
//	Status      string       `json:"status"`
//	Contestants []Contestant `json:"contestants"`
//	LiveStatus  int64        `json:"liveStatus"`
//}

type FixturesSpecialRequest struct {
	SportID   int64   `url:"sportId"`
	LeagueIDs []int   `url:"leagueIds"`
	Since     int64   `url:"since"`
	Category  *string `url:"category,omitempty"`
	EventId   *int64  `url:"eventId,omitempty"`
	SpecialId int64   `url:"specialId,omitempty"`
}

type Contestant struct {
	ID     int64  `json:"id"`
	Name   string `json:"name"`
	RotNum int64  `json:"rotNum"`
}

// ****************************
// *** fixtures settled ****
type FixturesSettledRequest struct {
	SportID   int64 `json:"sportId"`
	LeagueIDs []int `json:"leagueIds"`
	Since     int64 `json:"since"`
}

type FixturesSettledResponse struct {
	SportID int64    `json:"sportId"`
	Last    int64    `json:"last"`
	Leagues []League `json:"leagues"`
}

type LeagueSettled struct {
	ID     int64   `json:"id"`
	Events []Event `json:"events"`
}

type EventSettled struct {
	ID      int64    `json:"id"`
	Periods []Period `json:"periods"`
}

//type Period struct {
//	Number             int64               `json:"number"`
//	Status             int64               `json:"status"`
//	SettlementID       int64               `json:"settlementId"`
//	SettledAt          string              `json:"settledAt"`
//	Team1Score         int64               `json:"team1Score"`
//	Team2Score         int64               `json:"team2Score"`
//	Team1ScoreSets     int64               `json:"team1ScoreSets"`
//	Team2ScoreSets     int64               `json:"team2ScoreSets"`
//	CancellationReason *CancellationReason `json:"cancellationReason,omitempty"`
//}

type CancellationReason struct {
	Code        string `json:"code"`
	Description string `json:"description"`
}

//type CancellationReason struct {
//	Code    string  `json:"code"`
//	Details Details `json:"details"`
//}
//type CancellationReason struct {
//	Code    string   `json:"code"`
//	Details []Detail `json:"details"`
//}

type Details struct {
	CorrectTeam1Score *string `json:"correctTeam1Score,omitempty"`
	CorrectTeam2Score *string `json:"correctTeam2Score,omitempty"`
}

// **********************
// ***** Settled Special Fixtures
type FixturesSpecialSettledRequest struct {
	SportID   int64 `json:"sportId"`
	LeagueIDs []int `json:"leagueIds"`
	Since     int64 `json:"since"`
}

type FixturesSpecialSettledResponse struct {
	SportID int64                  `json:"sportId"`
	Last    int64                  `json:"last"`
	Leagues []LeagueSpecialSettled `json:"leagues"`
}

type LeagueSpecialSettled struct {
	ID       int64            `json:"id"`
	Specials []SpecialSettled `json:"specials"`
}

type SpecialSettled struct {
	ID           int64  `json:"id"`
	Status       int64  `json:"status"`
	SettlementID int64  `json:"settlementId"`
	SettledAt    string `json:"settledAt"`
}

type LineResponse struct {
	// NOT_EXISTS or SUCCESS
	Status        string  `json:"status"`
	Price         float64 `json:"price"`
	LineID        int64   `json:"lineId"`
	AltLineID     int64   `json:"altLineId"`
	Team1Score    int64   `json:"team1Score"`
	Team2Score    int64   `json:"team2Score"`
	Team1RedCards int64   `json:"team1RedCards"`
	Team2RedCards int64   `json:"team2RedCards"`
	MaxRiskStake  float64 `json:"maxRiskStake"`
	MinRiskStake  float64 `json:"minRiskStake"`
	MaxWinStake   float64 `json:"maxWinStake"`
	MinWinStake   float64 `json:"minWinStake"`
	EffectiveAsOf string  `json:"effectiveAsOf"`
}

type LineRequest struct {
	SportId    int64      `url:"sportId"`
	LeagueId   int64      `url:"leagueId"`
	EventId    int64      `url:"eventId"`
	OddsFormat OddsFormat `url:"oddsFormat"`
	BetType    BetType    `url:"betType"`
	// This represents the period of the match. For example, for soccer we have 0 (Game), 1 (1st Half) & 2 (2nd Half)
	PeriodNumber int64 `url:"periodNumber"`
	// Team Chosen Team type. This is needed only for SPREAD, MONEYLINE and TEAM_TOTAL_POINTS bet types
	Team TeamType `url:"team,omitempty"`
	// Chosen Side. This is needed only for TOTAL_POINTS and TEAM_TOTAL_POINTS
	Side Side `url:"side,omitempty"`
	// This is needed for SPREAD, TOTAL_POINTS and TEAM_TOTAL_POINTS bet types
	Handicap float64 `url:"handicap,omitempty"`
}
type SpecialLineRequest struct {
	SpecialId    int64      `url:"specialId"`
	ContestantId int64      `url:"contestantId"`
	OddsFormat   OddsFormat `url:"oddsFormat"`
	Handicap     float64    `url:"handicap,omitempty"`
}

// ** odds parlay
type LineParlayRequest struct {
	OddsFormat OddsFormat      `url:"oddsFormat,omitempty"`
	Legs       []LineParlayLeg `url:"legs,omitempty"`
}

type LineParlayLeg struct {
	UniqueLegId  string   `url:"uniqueLegId,omitempty"`
	EventId      int64    `url:"eventId,omitempty"`
	PeriodNumber int64    `url:"periodNumber,omitempty"` // 0 (Game), 1 (1st Half), 2 (2nd Half)
	LegBetType   BetType  `url:"legBetType,omitempty"`
	Team         TeamType `url:"team,omitempty"`
	Side         Side     `url:"side,omitempty"`
	Handicap     float32  `url:"handicap,omitempty"` //This is needed for SPREAD and TOTAL_POINTS bet type
}

// resp
type LineParlayResponse struct {
	Status                   string                    `json:"status"`
	Error                    string                    `json:"error"`
	MaxRiskStake             int64                     `json:"maxRiskStake"`
	MinRiskStake             int64                     `json:"minRiskStake"`
	RoundRobinOptionWithOdds []RoundRobinOptionWithOdd `json:"roundRobinOptionWithOdds"`
	Legs                     []Leg                     `json:"legs"`
}

//type Leg struct {
//	Status         string   `json:"status"`
//	ErrorCode      string   `json:"errorCode"`
//	LegID          string   `json:"legId"`
//	LineID         int64    `json:"lineId"`
//	AltLineID      int64    `json:"altLineId"`
//	Price          int64    `json:"price"`
//	CorrelatedLegs []string `json:"correlatedLegs"`
//}

type LineTeaserRequest struct {
	TeaserId   int64      `url:"teaserId,omitempty"`
	OddsFormat OddsFormat `url:"oddsFormat,omitempty"`

	Legs []LineParlayLeg
}

type LineTeaserResponse struct {
	Status       string      `json:"status"`
	ErrorCode    string      `json:"errorCode"`
	Price        int64       `json:"price"`
	MinRiskStake int64       `json:"minRiskStake"`
	MaxRiskStake int64       `json:"maxRiskStake"`
	MinWinStake  int64       `json:"minWinStake"`
	MaxWinStake  int64       `json:"maxWinStake"`
	Legs         []LegTeaser `json:"legs"`
}

type LegTeaser struct {
	Status    string `json:"status"`
	ErrorCode string `json:"errorCode"`
	LegID     string `json:"legId"`
	LineID    int64  `json:"lineId"`
}

type LineSpecialResponse struct {
	Status       string  `json:"status"`
	SpecialID    int64   `json:"specialId"`
	ContestantID int64   `json:"contestantId"`
	MinRiskStake float64 `json:"minRiskStake"`
	MaxRiskStake float64 `json:"maxRiskStake"`
	MinWinStake  float64 `json:"minWinStake"`
	MaxWinStake  float64 `json:"maxWinStake"`
	LineID       int64   `json:"lineId"`
	Price        float64 `json:"price"`
	Handicap     float64 `json:"handicap"`
}
type SportsResponse struct {
	Sports []Sport `json:"sports"`
}

type Sport struct {
	ID                  int64  `json:"id"`
	Name                string `json:"name"`
	HasOfferings        bool   `json:"hasOfferings"`
	LeagueSpecialsCount int64  `json:"leagueSpecialsCount"`
	EventSpecialsCount  int64  `json:"eventSpecialsCount"`
	EventCount          int64  `json:"eventCount"`
}

type LeaguesResponse struct {
	Leagues []League `json:"leagues"`
}

type League struct {
	ID                  int64    `json:"id"`
	Name                string   `json:"name"`
	HomeTeamType        TeamType `json:"homeTeamType"`
	HasOfferings        bool     `json:"hasOfferings"`
	Container           string   `json:"container"`
	AllowRoundRobins    bool     `json:"allowRoundRobins"`
	LeagueSpecialsCount int64    `json:"leagueSpecialsCount"`
	EventSpecialsCount  int64    `json:"eventSpecialsCount"`
	EventCount          int64    `json:"eventCount"`
}

type PeriodsResponse struct {
	Periods []Period `json:"periods"`
}

type Period struct {
	Number                     int64  `json:"number"`
	Description                string `json:"description"`
	ShortDescription           string `json:"shortDescription"`
	SpreadDescription          string `json:"spreadDescription"`
	MoneylineDescription       string `json:"moneylineDescription"`
	TotalDescription           string `json:"totalDescription"`
	Team1TotalDescription      string `json:"team1TotalDescription"`
	Team2TotalDescription      string `json:"team2TotalDescription"`
	SpreadShortDescription     string `json:"spreadShortDescription"`
	MoneylineShortDescription  string `json:"moneylineShortDescription"`
	TotalShortDescription      string `json:"totalShortDescription"`
	Team1TotalShortDescription string `json:"team1TotalShortDescription"`
	Team2TotalShortDescription string `json:"team2TotalShortDescription"`
}

type InrunningResponse struct {
	Sports []InrunningSport `json:"sports"`
}

type InrunningSport struct {
	ID      int64             `json:"id"`
	Leagues []InrunningLeague `json:"leagues"`
}

type InrunningLeague struct {
	ID     int64            `json:"id"`
	Events []InrunningEvent `json:"events"`
}

type InrunningEvent struct {
	ID      int64 `json:"id"`
	State   int64 `json:"state"`
	Elapsed int64 `json:"elapsed"`
}

type TeaserGroupsResponse struct {
	TeaserGroups []TeaserGroup `json:"teaserGroups"`
}

type TeaserGroup struct {
	ID      int64    `json:"id"`
	Name    string   `json:"name"`
	Teasers []Teaser `json:"teasers"`
}

type Teaser struct {
	ID            int64          `json:"id"`
	Description   string         `json:"description"`
	SportID       int64          `json:"sportId"`
	MinLegs       int64          `json:"minLegs"`
	MaxLegs       int64          `json:"maxLegs"`
	SameEventOnly bool           `json:"sameEventOnly"`
	Payouts       []Payout       `json:"payouts"`
	Leagues       []LeagueTeaser `json:"leagues"`
}

//type LeagueTeaser struct {
//	ID     int64  `json:"id"`
//	Spread Spread `json:"spread"`
//	Total  Spread `json:"total"`
//}

//type Spread struct {
//	Points float64 `json:"points"`
//}

type Payout struct {
	NumberOfLegs int64   `json:"numberOfLegs"`
	Price        float64 `json:"price"`
}

type CancellationReasonsResponse struct {
	CancellationReasons []CancellationReason `json:"cancellationReasons"`
}

type CurrenciesResponse struct {
	Currencies []Currency `json:"currencies"`
}
type Currency struct {
	Code string  `json:"code"`
	Name string  `json:"name"`
	Rate float64 `json:"rate"`
}
type OddsResponse struct {
	SportID int64        `json:"sportId"`
	Last    int64        `json:"last"`
	Leagues []OddsLeague `json:"leagues"`
}

type OddsLeague struct {
	ID     int64       `json:"id"`
	Events []OddsEvent `json:"events"`
}

type OddsEvent struct {
	ID      int64        `json:"id"`
	Periods []OddsPeriod `json:"periods"`
}

type OddsPeriod struct {
	LineID             int64      `json:"lineId"`
	Number             int64      `json:"number"`
	Cutoff             time.Time  `json:"cutoff"`
	MoneylineUpdatedAt *time.Time `json:"moneylineUpdatedAt,omitempty"`
	SpreadUpdatedAt    *time.Time `json:"spreadUpdatedAt,omitempty"`
	TotalUpdatedAt     *time.Time `json:"totalUpdatedAt,omitempty"`
	TeamTotalUpdatedAt *time.Time `json:"teamTotalUpdatedAt,omitempty"`
	MaxMoneyline       *float64   `json:"maxMoneyline,omitempty"`
	MaxSpread          *float64   `json:"maxSpread,omitempty"`
	MaxTotal           *float64   `json:"maxTotal,omitempty"`
	MaxTeamTotal       *float64   `json:"maxTeamTotal,omitempty"`
	Moneyline          *Moneyline `json:"moneyline,omitempty"`
	Spreads            []*Spread  `json:"spreads,omitempty"`
	Totals             []*Total   `json:"totals,omitempty"`
	TeamTotal          *TeamTotal `json:"teamTotal,omitempty"`
	Status             int64      `json:"status"`
}

type Moneyline struct {
	Home float64 `json:"home"`
	Away float64 `json:"away"`
	Draw float64 `json:"draw"`
}

type Spread struct {
	Hdp       float64  `json:"hdp"`
	Home      float64  `json:"home"`
	Away      float64  `json:"away"`
	AltLineID *int64   `json:"altLineId,omitempty"`
	MaxSpread *float64 `json:"max,omitempty"`
}

type Total struct {
	Points    float64 `json:"points"`
	Over      float64 `json:"over"`
	Under     float64 `json:"under"`
	AltLineID *int64  `json:"altLineId,omitempty"`
	//Maximum bet volume. Present only on alternative lines, if set it overrides maxTotal market limit.
	MaxTotal *float64 `json:"max,omitempty"`
}

type TeamTotal struct {
	Home *Total `json:"home,omitempty"`
	Away *Total `json:"away,omitempty"`
}

type OddsRequest struct {
	SportID    int64      `url:"sportId"`
	LeagueIds  []int      `url:"leagueIds"`
	OddsFormat OddsFormat `url:"oddsFormat,omitempty"`
	Since      int64      `url:"since,omitempty"`
	IsLive     bool       `url:"isLive,omitempty"`
	EventIds   []int      `url:"eventIds,omitempty"`
	// Default USD.
	ToCurrencyCode string `url:"toCurrencyCode,omitempty"`
}

type OddsParlayResponse struct {
	SportID int64          `json:"sportId"`
	Last    int64          `json:"last"`
	Leagues []LeagueParlay `json:"leagues"`
}

type LeagueParlay struct {
	ID     int64         `json:"id"`
	Events []EventParlay `json:"events"`
}

type EventParlay struct {
	ID           int64          `json:"id"`
	AwayScore    float64        `json:"awayScore"`
	HomeScore    float64        `json:"homeScore"`
	AwayRedCards int64          `json:"awayRedCards"`
	HomeRedCards int64          `json:"homeRedCards"`
	Periods      []PeriodParlay `json:"periods"`
}

type PeriodParlay struct {
	LineID       int64           `json:"lineId"`
	Number       int64           `json:"number"`
	Cutoff       string          `json:"cutoff"`
	Status       int64           `json:"status"`
	MaxSpread    float64         `json:"maxSpread"`
	MaxMoneyline float64         `json:"maxMoneyline"`
	MaxTotal     float64         `json:"maxTotal"`
	MaxTeamTotal float64         `json:"maxTeamTotal"`
	Spreads      []SpreadParlay  `json:"spreads"`
	Moneyline    MoneylineParlay `json:"moneyline"`
	Totals       []Total         `json:"totals"`
	TeamTotal    TeamTotalParlay `json:"teamTotal"`
}

type MoneylineParlay struct {
	Home float64 `json:"home"`
	Away float64 `json:"away"`
	Draw float64 `json:"draw"`
}

type SpreadParlay struct {
	AltLineID int64   `json:"altLineId"`
	Hdp       float64 `json:"hdp"`
	Home      float64 `json:"home"`
	Away      float64 `json:"away"`
}

type TeamTotalParlay struct {
	Away AwayParlay `json:"away"`
	Home AwayParlay `json:"home"`
}

type AwayParlay struct {
	AltLineID int64   `json:"altLineId"`
	Points    int64   `json:"points"`
	Over      float64 `json:"over"`
	Under     float64 `json:"under"`
}

//**** Odds teaser
type OddsTeaserResponse struct {
	TeaserID int64          `json:"teaserId"`
	SportID  int64          `json:"sportId"`
	Leagues  []LeagueTeaser `json:"leagues"`
}

type LeagueTeaser struct {
	ID     int64         `json:"id"`
	Events []EventTeaser `json:"events"`
}

type EventTeaser struct {
	ID      int64          `json:"id"`
	Periods []PeriodTeaser `json:"periods"`
}

type PeriodTeaser struct {
	Number int64        `json:"number"`
	LineID int64        `json:"lineId"`
	Spread SpreadTeaser `json:"spread"`
	Total  TotalTeaser  `json:"total"`
}

type SpreadTeaser struct {
	MaxBet  int64   `json:"maxBet"`
	HomeHdp float64 `json:"homeHdp"`
	AwayHdp float64 `json:"awayHdp"`
	AltHdp  bool    `json:"altHdp"`
}

type TotalTeaser struct {
	MaxBet      int64   `json:"maxBet"`
	OverPoints  float64 `json:"overPoints"`
	UnderPoints float64 `json:"underPoints"`
}
type OddsSpecialRequest struct {
	OddsFormat OddsFormat `url:"oddsFormat,omitempty"`
	SportID    int        `url:"sportId,omitempty"`
	LeagueIDs  []int      `url:"leagueIds,omitempty"`
	Since      int        `url:"since,omitempty"`
	SpecialID  []int      `url:"specialId"`
}

type OddsSpecialResponse struct {
	SportID int64           `json:"sportId"`
	Last    int64           `json:"last"`
	Leagues []LeagueSpecial `json:"leagues"`
}

type LeagueSpecial struct {
	ID       int64     `json:"id"`
	Specials []Special `json:"specials"`
}

type Special struct {
	ID              int64            `json:"id"`
	MaxBet          int64            `json:"maxBet"`
	ContestantLines []ContestantLine `json:"contestantLines"`
}

type ContestantLine struct {
	ID       int64       `json:"id"`
	LineID   int64       `json:"lineId"`
	Price    int64       `json:"price"`
	Handicap interface{} `json:"handicap"`
}
