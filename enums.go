package pinapi

type BetType string

const (
	MONEYLINE BetType = "MONEYLINE"
	TEAMTOTAL BetType = "TEAM_TOTAL_POINTS"
	SPREAD    BetType = "SPREAD"
	TOTAL     BetType = "TOTAL_POINTS"
)

type Side string

const (
	OVER  Side = "OVER"
	UNDER Side = "UNDER"
)

type TeamType string

const (
	TEAM1 TeamType = "Team1"
	TEAM2 TeamType = "Team2"
	DRAW  TeamType = "Draw"
)

type BetListType string

const (
	Settled        BetListType = "SETTLED"
	Running        BetListType = "RUNNING"
	Cancelled      BetListType = "CANCELLED"
	BetListTypeAll BetListType = "ALL"
)

type EventStatus string

const (
	Unavailable EventStatus = "H"
	LowerMax    EventStatus = "I"
	Open        EventStatus = "O"
)

type OddsFormat string

const (
	DECIMAL    OddsFormat = "DECIMAL"
	AMERICAN   OddsFormat = "AMERICAN"
	HONGKONG   OddsFormat = "HONGKONG"
	INDONESIAN OddsFormat = "INDONESIAN"
	MALAY      OddsFormat = "MALAY"
)

type WinRiskType string

const (
	Win  WinRiskType = "WIN"
	Risk WinRiskType = "RISK"
)

type FillType string

const (
	Normal       FillType = "NORMAL"
	FillAndKill  FillType = "FILLANDKILL"
	FillMaxLimit FillType = "FILLMAXLIMIT"
)

type BetStatusesType string

const (
	Won                      BetStatusesType = "WON"
	Lose                     BetStatusesType = "LOSE"
	BetStatusesTypeCancelled BetStatusesType = "CANCELLED"
	Refunded                 BetStatusesType = "REFUNDED"
	NotAccepted              BetStatusesType = "NOT_ACCEPTED"
	Accepted                 BetStatusesType = "ACCEPTED"
	PendingAcceptance        BetStatusesType = "PENDING_ACCEPTANCE"
)

type SortDirType string

const (
	Asc  SortDirType = "ASC"
	Desc SortDirType = "DESC"
)
